const http = require('http');

var port = process.env.PORT || 1337;

var server = http.createServer((request, response) => {
  response.statusCode = 200;
  response.setHeader('Content-Type', 'text/plain');
  response.end('Welcome to Template NodeJS Web App');
});

server.listen(port, () => {
  console.log("Server running at http://localhost:%d", port);
});